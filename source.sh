# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://github.com/NetworkConfiguration/dhcpcd/releases/download/v${BPM_PKG_VERSION}/dhcpcd-${BPM_PKG_VERSION}.tar.xz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  ./configure --prefix=/usr                \
              --sysconfdir=/etc            \
              --libexecdir=/usr/lib/dhcpcd \
              --dbdir=/var/lib/dhcpcd      \
              --runstatedir=/run           \
              --disable-privsep            \
  make
}

# The check function is executed in the source directory
# This function is used to run tests to verify the package has been compiled correctly
check() {
  make test
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  make DESTDIR="$BPM_OUTPUT" install
  mkdir -p "$BPM_OUTPUT"/usr/lib/systemd/system
  install -m 644 ../dhcpcdat "$BPM_OUTPUT"/usr/lib/systemd/system/dhcpcd@.service
  install -m 644 ../dhcpcd "$BPM_OUTPUT"/usr/lib/systemd/system/dhcpcd.service
}
